Load Balancing Algorithms in Kubernetes:
Round-robin (default):

This is the default load balancing algorithm for Services in Kubernetes.
Requests are distributed sequentially across Pods that match the Service's selector.
Session Affinity:

Kubernetes supports session affinity (also known as sticky sessions) using cookies.
Sessions can be "clientIP" or "None". "ClientIP" ensures that requests from the same client IP address are always routed to the same Pod.

example 
apiVersion: v1
kind: Service
metadata:
  name: my-service
spec:
  selector:
    app: nginx
  ports:
    - protocol: TCP
      port: 80
      targetPort: 80
  sessionAffinity: ClientIP  # Enable client IP based session affinity


Conclusion:
While Kubernetes abstracts the details of load balancing algorithms, it ensures that requests are distributed evenly across Pods that match the Service's selector. For finer control over load balancing behaviors, especially in cloud environments, consider using annotations provided by your cloud provider or Kubernetes distribution, or explore custom configurations through advanced networking setups or ingress controllers.


Read more : https://romanglushach.medium.com/kubernetes-networking-load-balancing-techniques-and-algorithms-5da85c5c7253


