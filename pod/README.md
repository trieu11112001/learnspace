1 . POD 
kubectl run nginx-pod --image=nginx --port=80 --labels="appname=nginx,apptype=web" --dry-run=client -oyaml > nginx-pod.yaml
kubectl apply -f nginx-pod.yaml ( hoặc kubectl create -f nginx-pod.yaml --save-config) nếu không có --save-config 
thì sẽ không có cấu hình ghi vào file yaml và việc cập nhật pod sẽ k apply được lỗi sẽ như sau 
;  ( Warning: resource pods/nginx-pod is missing the kubectl.kubernetes.io/last-applied-configuration annotation
;  which is required by kubectl apply. kubectl apply should only be used on resources created declaratively by
;   either kubectl create --save-config or kubectl apply. The missing annotation will be patched automatically. )
thì không dùng apply được nữa mà phải xoá pod tạo lại hoặc sử dụng kubectl replace -f nginx-pod.yaml hoặc kubectl edit pod nginx-pod
kubectl replace thay thế hoàn toàn đối tượng hiện có bằng định nghĩa mới. Nếu có sự thay đổi lớn trong cấu hình, đối tượng hiện có sẽ bị xóa và tạo lại.
kubectl apply áp dụng các thay đổi từng phần và giữ nguyên những phần không thay đổi của đối tượng hiện có. Nếu đối tượng chưa tồn tại, nó sẽ được tạo ra.
kubectl edit chỉnh sửa đối tượng hiện có bằng cách mở trình soạn thảo mặc định của bạn để chỉnh sửa cấu hình của đối tượng không dựa vào file yml gốc( như nút edit trong len )
    
    
    Thêm cấu hình "--dry-run=client -oyaml" để xem trước cấu hình được gen ra chứ không apply vào hệ thống ngay:
    > nginx-pod.yaml là để ghi vào file yaml

    Một số lệnh cơ bản
Lấy danh sách Pod, nếu không chỉ định namesapce thì mặc định sẽ lấy ở default namespace:

# Liệt kê các pod ở namespace mặc định
kubectl get pods

# Hiện thị nhiều thông tin hơn
kubectl get pod -o wide

# Hiện thị thông tin pod kèm label
kubectl get pod -o wide --show-labels

# Pod ở namepace: kubernetes-dashboard
kubectl get pod -o wide -n namespace-name

# Liệt kê các Pod có nhãn "app:pod-name"
kubectl get pod -l "app=pod-name"

# Pod ở tất cả các namespace
kubectl get pod -A

Xem thông tin mô tả chi tiết của Pod:

kubectl describe pod/namepod

Theo dõi log của Pod:

#Log của pod tên là pod-name
kubectl logs pod/pod-name
#Log tất cả các Pod có label là app=pod-name
kubectl logs -l "app=pod-name"

Exec vào trong Pod để chạy lệnh, nếu có nhiều container trong Pod thì chỉ định container bằng tham số -c:

kubectl exec pod-name command
kubectl exec pod-name -c container-name command

Chạy lệnh bash của container trong POD pod-name và gắn terminal:

kubectl exec -it pod-name bash

Xóa Pod:

#Xóa pod theo tên
kubectl delete pod/pod-name

#Xóa pod bằng chính file khai báo pod
kubectl delete -f pod-name.yaml
