ReplicaSet có thể được tạo thủ công bằng cách khai báo các file yaml. Nó cũng được tự động tạo ra khi ta tạo hay update Deployment.



Cơ chế quản lý ReplicaSet và Pod của Deployment
Như đã đề cập bên trên, khi ta tạo mới một Deployment thì hệ thống sẽ sinh ra 1 ReplicaSet tương ứng, ReplicaSet này sẽ làm nhiệm vụ tạo ra các Pod. Vậy câu hỏi đặt ra là làm thế nào Deployment quản lý được các ReplicaSet của nó, và làm thế nào ReplicaSet quản lý các Pod của nó?

Câu trả lời là chúng sử dụng labels để quản lý.

DEPLOYMENT DEFAULT STRATEGY

![alt text](image.png)
![alt text](image-1.png)


HOW TO UPDATE 
1. APPLY
2. SET 
3. EDIT
4. rollout

  # Restart all deployments in test-namespace namespace
  kubectl rollout restart deployment -n test-namespace
  
  # Restart a deployment
  kubectl rollout restart deployment/nginx

  # Restart a daemon set
  kubectl rollout restart daemonset/abc
  
  # Restart deployments with the app=nginx label
  kubectl rollout restart deployment --selector=app=nginx
<!-- (trạng thái update) -->
kubectl rollout status deployment myapp-deployment 

<!-- (lịch sử update) -->
kubectl rollout history deployment myapp-deployment 

<!-- Thêm casue cho history rollout -->
kubectl annotate deployment nginx kubernetes.io/change-cause="version change to 1.16.0 to latest" --overwrite=true 
hooặc thêm flag --record vào lệnh update

 <!-- (rollback về 1 phiên bản ) -->
kubectl rollout undo deployment myapp-deployment



kubectl rollout undo deployment myapp-deployment --to-revision=<revision>


mỗi khi rollout deployment sẽ xem có phiên bản đó chưa rồi tạo một replicaset khác nếu sửa những gì mà replicaset cho phép thì nó sẽ k tạo mà thực hiện edit lại replcaset đó 
